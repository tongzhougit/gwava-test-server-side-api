## Web project 

##### RESTFUL API Server Side

###### Description: 
provide json data forllow JAX-RS standard

###### Prerequisite:
Create database and update the following filed in config (The upper case varible) 
```
spring.datasource.url=jdbc:mysql://DATABASE_HOST/YDATABSE_NAME?zeroDateTimeBehavior=convertToNull
spring.datasource.username=DATABASE_USERNAME
spring.datasource.password=DATABASE_PASSWORD
```

 under files:
* For Application: ```src/main/resources/application.properties```
* For Test: ```src/main/resources/application-test.properties```


###### System Requirment: 
Project require Java 8

###### Reference: 
Project is developed using Spring boot 1.3.5

###### Quick run project: 
``` 
./gradlew bootRun 
```
Notice: API will be run under port: 7799, you can modify it by overriding the config.

###### Build deployable war:
``` 
./gradlew clean build 
```
The war file will be in ``` build/libs/server-side-api.war ```

###### Test:
``` ./gradlew clean test ```

###### Generate Eclipse project:
``` ./gradlew clean eclipse ```

###### Example endpoints:
* localhost:7799/setup/customer (setup customer table)
* localhost:7799/customers  (get list of customers)
* localhost:7799/customers/1  (get customers id 1)
etc..