package api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/** 
 * Database init service 
 * 
 * @author Tong Zhou
 * @version 
 */
@Service
public class DatabaseInitService extends AbstractService
{
    @Autowired
    JdbcTemplate jdbcTemplate;

    /** 
     * Covert sample data to insertable data 
     * 
     * @param sample 
     * @return 
     */
    public List<Object[]> getSampleData(String[] sample)
    {
        return Arrays.stream(sample)
            .map(item -> (Object[]) item.split(":"))
            .collect(Collectors.toList()); 
    }

    /**
     * Apply setup of table + insert sample data
     *
     * @param dropTableQuery
     * @param createTableQuery
     * @param batchInsertQuery
     * @param initData
     * @return
     */
    public boolean apply(
        String dropTableQuery,
        String createTableQuery,
        String batchInsertQuery,
        List<Object[]> initData
    )
    {
        jdbcTemplate.execute(dropTableQuery);
        jdbcTemplate.execute(createTableQuery);
        jdbcTemplate.batchUpdate(batchInsertQuery, initData);
        return true;
    }
}
