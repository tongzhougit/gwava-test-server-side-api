package api.response;

/**
 * Response mapper
 *
 * @author Tong Zhou
 */
public class Mapper<T>
{
    private T content;


    /**
     * Constructor
     */
    public Mapper(T content) 
    {
        this.content = content;
    }

    /**
     * @return the content
     */
    public T getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(T content) {
        this.content = content;
    }
}
