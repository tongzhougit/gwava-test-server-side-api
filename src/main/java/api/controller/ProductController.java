package api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import api.repository.ProductRepository;
import api.response.Mapper;
import api.domain.Product;

/** 
 * Customer controller 
 * 
 * @author Tong Zhou
 * @version 
 */
@RestController
@RequestMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController
{
    @Autowired
    private ProductRepository productRepository;

    /** 
     * get list of products
     * 
     * @return List of customers
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> findAll()
    {
        return new ResponseEntity<>(
            new Mapper<Iterable<Product>>(productRepository.findAll()), 
            HttpStatus.OK
        );
    }

     /** 
      * get product by id 
      * 
      * @param id product id
      * @return one customer
      */
     @RequestMapping(value = "/{id}", method = RequestMethod.GET)
     public ResponseEntity<?> findOne(@PathVariable long id)
     {
         return new ResponseEntity<>(new Mapper<Product>(productRepository.findOne(id)), HttpStatus.OK);
     }
} 
