package api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import api.repository.OrderRepository;
import api.response.Mapper;
import api.domain.Order;

/** 
 * Order controller 
 * 
 * @author Tong Zhou
 * @version 
 */
@RestController
@RequestMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController
{
    @Autowired
    private OrderRepository orderRepository;

    /** 
     * get list of orders
     * 
     * @return List of orders
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> findAll()
    {
        return new ResponseEntity<>(new Mapper<Iterable<Order>>(orderRepository.findAll()), HttpStatus.OK);
    }

     /** 
      * get by id 
      * 
      * @param id order id
      * @return one order
      */
     @RequestMapping(value = "/{id}", method = RequestMethod.GET)
     public ResponseEntity<?> findOne(@PathVariable long id)
     {
        return new ResponseEntity<>(new Mapper<Order>(orderRepository.findOne(id)), HttpStatus.OK);
     }
} 
