package api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import api.service.DatabaseInitService;
import api.domain.Customer;
import api.domain.Product;
import api.domain.Order;
import api.response.Mapper;

/** 
 * Setup controller 
 * 
 * @author Tong Zhou
 * @version 
 */
@RestController
@RequestMapping(value = "/setup", produces = MediaType.APPLICATION_JSON_VALUE)
public class SetupController
{
    @Autowired
    private DatabaseInitService databaseInitService;
    
    @Value("${sample.customer}")
    private String[] customerSample;

    @Value("${sample.product}")
    private String[] productSample;

    @Value("${sample.order}")
    private String[] orderSample;
    
    /** 
     * setup customer table 
     * 
     * @return boolean
     */
    @RequestMapping(value = "/customer",method = RequestMethod.GET)
    public ResponseEntity<?> customer()
    {
        Customer customer = new Customer();
        customer.setInitData(databaseInitService.getSampleData(customerSample));
        boolean result = databaseInitService.apply(
            customer.getDropTableQuery(),
            customer.getCreateTableQuery(),
            customer.getBatchInsertQuery(),
            customer.getInitData()
        );
        return new ResponseEntity<>(new Mapper<Boolean>(result), HttpStatus.OK);
    }

    /** 
     * setup product table 
     * 
     * @return boolean
     */
    @RequestMapping(value = "/product",method = RequestMethod.GET)
    public ResponseEntity<?> product()
    {
        Product product = new Product();
        product.setInitData(databaseInitService.getSampleData(productSample));
        boolean result = databaseInitService.apply(
            product.getDropTableQuery(),
            product.getCreateTableQuery(),
            product.getBatchInsertQuery(),
            product.getInitData()
        );
        return new ResponseEntity<>(new Mapper<Boolean>(result), HttpStatus.OK);
    }

    /** 
     * setup order table 
     * 
     * @return boolean
     */
    @RequestMapping(value = "/order",method = RequestMethod.GET)
    public ResponseEntity<?> order()
    {
        Order order = new Order();
        order.setInitData(databaseInitService.getSampleData(orderSample));
        boolean result = databaseInitService.apply(
            order.getDropTableQuery(),
            order.getCreateTableQuery(),
            order.getBatchInsertQuery(),
            order.getInitData()
        );
        return new ResponseEntity<>(new Mapper<Boolean>(result), HttpStatus.OK);
    }
} 
