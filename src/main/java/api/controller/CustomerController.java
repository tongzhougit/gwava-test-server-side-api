package api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import api.repository.CustomerRepository;
import api.domain.Customer;
import api.response.Mapper;

/** 
 * Customer controller 
 * 
 * @author Tong Zhou
 * @version 
 */
@RestController
@RequestMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController
{
    @Autowired
    private CustomerRepository customerRepository;

    
    /** 
     * get list of customers 
     * 
     * @return List of customers
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> findAll()
    {
        return new ResponseEntity<>(
            new Mapper<Iterable<Customer>>(customerRepository.findAll()), 
            HttpStatus.OK
        );
    }

     /** 
      * get customer by id 
      * 
      * @param id customer id
      * @return one customer
      */
     @RequestMapping(value = "/{id}", method = RequestMethod.GET)
     public ResponseEntity<?> findOne(@PathVariable long id)
     {
         return new ResponseEntity<>(new Mapper<Customer>(customerRepository.findOne(id)), HttpStatus.OK);
     }
} 
