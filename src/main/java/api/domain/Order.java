package api.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/** 
 * Entity for order
 * 
 * @author Tong Zhou
 * @version 
 */
@Entity
@Table(name="orders")
public class Order implements Serializable 
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @Transient
    private String dropTableQuery = "DROP TABLE IF EXISTS orders";

    @JsonIgnore
    @Transient
    private String createTableQuery = "CREATE TABLE orders(id SERIAL, customer_id bigint, total_price float)";

    @JsonIgnore
    @Transient
    private String batchInsertQuery = "INSERT INTO orders(customer_id, total_price) VALUES (?, ?)";

    @JsonIgnore
    @Transient
    private List<Object[]> initData;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "customer_id")
    private long customerId;

    @Column(name = "total_price")
    private double totalPrice;

    /** 
     * constructor 
     * 
     * @param id 
     * @param customerId
     * @param totalPrice 
     */
    public Order(long id, long customerId, double totalPrice) {
        this.id = id;
        this.customerId = customerId;
        this.totalPrice = totalPrice;
    }

    public Order()
    {
    }

    /**
     * @return the dropTableQuery
     */
    public String getDropTableQuery() {
        return dropTableQuery;
    }

    /**
     * @param dropTableQuery the dropTableQuery to set
     */
    public void setDropTableQuery(String dropTableQuery) {
        this.dropTableQuery = dropTableQuery;
    }

    /**
     * @return the createTableQuery
     */
    public String getCreateTableQuery() {
        return createTableQuery;
    }

    /**
     * @param createTableQuery the createTableQuery to set
     */
    public void setCreateTableQuery(String createTableQuery) {
        this.createTableQuery = createTableQuery;
    }

    /**
     * @return the batchInsertQuery
     */
    public String getBatchInsertQuery() {
        return batchInsertQuery;
    }

    /**
     * @param batchInsertQuery the batchInsertQuery to set
     */
    public void setBatchInsertQuery(String batchInsertQuery) {
        this.batchInsertQuery = batchInsertQuery;
    }

    /**
     * @return the initData
     */
    public List<Object[]> getInitData() {
        return initData;
    }

    /**
     * @param initData the initData to set
     */
    public void setInitData(List<Object[]> initData) {
        this.initData = initData;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the customerId
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

}
