package api.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/** 
 * Entity for customer 
 * 
 * @author Tong Zhou
 * @version 
 */
@Entity
@Table(name="customers")
public class Customer implements Serializable 
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @Transient
    private String dropTableQuery = "DROP TABLE IF EXISTS customers";

    @JsonIgnore
    @Transient
    private String createTableQuery = "CREATE TABLE customers(id SERIAL, first_name VARCHAR(255), last_name VARCHAR(255))";

    @JsonIgnore
    @Transient
    private String batchInsertQuery = "INSERT INTO customers(first_name, last_name) VALUES (? ,?)";

    @JsonIgnore
    @Transient
    private List<Object[]> initData;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = true, updatable = true, nullable = false)
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    /** 
     * constructor 
     */
    public Customer()
    {
    }

    /** 
     * constructor 
     * 
     * @param id 
     * @param firstName 
     * @param lastName 
     */
    public Customer(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return String.format("Customer[id=%d, firstName='%s', lastName='%s']", id, firstName, lastName);
    }

    /**
     * @return the dropTableQuery
     */
    public String getDropTableQuery() {
        return dropTableQuery;
    }

    /**
     * @param dropTableQuery the dropTableQuery to set
     */
    public void setDropTableQuery(String dropTableQuery) {
        this.dropTableQuery = dropTableQuery;
    }

    /**
     * @return the createTableQuery
     */
    public String getCreateTableQuery() {
        return createTableQuery;
    }

    /**
     * @param createTableQuery the createTableQuery to set
     */
    public void setCreateTableQuery(String createTableQuery) {
        this.createTableQuery = createTableQuery;
    }

    /**
     * @return the batchInsertQuery
     */
    public String getBatchInsertQuery() {
        return batchInsertQuery;
    }

    /**
     * @param batchInsertQuery the batchInsertQuery to set
     */
    public void setBatchInsertQuery(String batchInsertQuery) {
        this.batchInsertQuery = batchInsertQuery;
    }

    /**
     * @return the initData
     */
    public List<Object[]> getInitData() {

        return initData;
    }

    /**
     * @param initData the initData to set
     */
    public void setInitData(List<Object[]> initData) {
        this.initData = initData;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
