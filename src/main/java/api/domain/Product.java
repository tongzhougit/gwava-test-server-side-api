package api.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/** 
 * Entity for products
 * 
 * @author Tong Zhou
 * @version 
 */
@Entity
@Table(name="products")
public class Product implements Serializable 
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @Transient
    private String dropTableQuery = "DROP TABLE IF EXISTS products";

    @JsonIgnore
    @Transient
    private String createTableQuery = "CREATE TABLE products(id SERIAL, title VARCHAR(255), description VARCHAR(255), price float)";

    @JsonIgnore
    @Transient
    private String batchInsertQuery = "INSERT INTO products(title, description, price) VALUES (?, ?, ?)";

    @JsonIgnore
    @Transient
    private List<Object[]> initData;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private double price;

    /** 
     * constructor 
     * 
     * @param id 
     * @param title
     * @param description
     * @param price 
     */
    public Product(long id, String title, String description, double price) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
    }


    public Product()
    {
    }

    /**
     * @return the dropTableQuery
     */
    public String getDropTableQuery() {
        return dropTableQuery;
    }

    /**
     * @param dropTableQuery the dropTableQuery to set
     */
    public void setDropTableQuery(String dropTableQuery) {
        this.dropTableQuery = dropTableQuery;
    }

    /**
     * @return the createTableQuery
     */
    public String getCreateTableQuery() {
        return createTableQuery;
    }

    /**
     * @param createTableQuery the createTableQuery to set
     */
    public void setCreateTableQuery(String createTableQuery) {
        this.createTableQuery = createTableQuery;
    }

    /**
     * @return the batchInsertQuery
     */
    public String getBatchInsertQuery() {
        return batchInsertQuery;
    }

    /**
     * @param batchInsertQuery the batchInsertQuery to set
     */
    public void setBatchInsertQuery(String batchInsertQuery) {
        this.batchInsertQuery = batchInsertQuery;
    }

    /**
     * @return the initData
     */
    public List<Object[]> getInitData() {
        return initData;
    }

    /**
     * @param initData the initData to set
     */
    public void setInitData(List<Object[]> initData) {
        this.initData = initData;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

}
