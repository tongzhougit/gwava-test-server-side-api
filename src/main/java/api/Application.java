package api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.builder.SpringApplicationBuilder;  


/** 
 * Application to boostrap 
 * 
 * @author Tong Zhou
 * @version 
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer 
{
    
    /** 
     * application start main 
     * 
     * @param args 
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception 
    {
        SpringApplication.run(Application.class, args);
    }

    
    /** 
     * configration load builder
     * 
     * @param application 
     * @return 
     */
    @Override
    protected final SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}
