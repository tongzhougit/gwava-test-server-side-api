package api.repository;

import api.domain.Customer;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
      
/** 
 * Customer Repo
 * 
 * @author Tong Zhou
 * @version 
 */
public interface CustomerRepository extends CrudRepository<Customer, Long>
{
}   
