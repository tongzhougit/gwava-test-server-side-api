package api.repository;

import api.domain.Order;
import org.springframework.data.repository.CrudRepository;                      

/** 
 * Order Repo
 * 
 * @author Tong Zhou
 * @version 
 */     
public interface OrderRepository extends CrudRepository<Order, Long>
{
}   
