package api.repository;

import api.domain.Product;
import org.springframework.data.repository.CrudRepository;                      
      
/** 
 * Product Repo
 * 
 * @author Tong Zhou
 * @version 
 */
public interface ProductRepository extends CrudRepository<Product, Long>
{
}   
