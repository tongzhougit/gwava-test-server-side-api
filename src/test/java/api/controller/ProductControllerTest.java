package api.controller;

import api.Application;
import api.domain.Product;
import api.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.nio.charset.Charset;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class ProductControllerTest 
{
    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();;
        jdbcTemplate.execute("truncate products");
        productRepository.save(new Product((long) 1, "Sony TV", "televsion for sale", 1999));
        productRepository.save(new Product((long) 2, "Iphone 6", "Iphone 6 32G", 599.99));
        productRepository.save(new Product((long) 3, "LG phone", "LG 16G", 400.20));
        productRepository.save(new Product((long) 4, "PS4", "Play station 4", 399));
    }

    @Test
    public void findAll() throws Exception 
    {
        this.mockMvc.perform( get("/products")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.content", hasSize(4)))

            .andExpect(jsonPath("$.content[0].id", is(1)))
            .andExpect(jsonPath("$.content[0].title", is("Sony TV")))
            .andExpect(jsonPath("$.content[0].description", is("televsion for sale")))
            .andExpect(jsonPath("$.content[0].price", is((double) 1999)))

            .andExpect(jsonPath("$.content[1].id", is(2)))
            .andExpect(jsonPath("$.content[1].title", is("Iphone 6")))
            .andExpect(jsonPath("$.content[1].description", is("Iphone 6 32G")))
            .andExpect(jsonPath("$.content[1].price", is((double) 599.99)))

            .andExpect(jsonPath("$.content[2].id", is(3)))
            .andExpect(jsonPath("$.content[2].title", is("LG phone")))
            .andExpect(jsonPath("$.content[2].description", is("LG 16G")))
            .andExpect(jsonPath("$.content[2].price", is((double) 400.20)))

            .andExpect(jsonPath("$.content[3].id", is(4)))
            .andExpect(jsonPath("$.content[3].title", is("PS4")))
            .andExpect(jsonPath("$.content[3].description", is("Play station 4")))
            .andExpect(jsonPath("$.content[3].price", is((double) 399)));

    }

    @Test
    public void findOne() throws Exception 
    {
        this.mockMvc.perform(get("/products/1")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            .andExpect(jsonPath("$.content.id", is(1)))
            .andExpect(jsonPath("$.content.title", is("Sony TV")))
            .andExpect(jsonPath("$.content.description", is("televsion for sale")))
            .andExpect(jsonPath("$.content.price", is((double) 1999)));

        this.mockMvc.perform(get("/products/2")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            .andExpect(jsonPath("$.content.id", is(2)))
            .andExpect(jsonPath("$.content.title", is("Iphone 6")))
            .andExpect(jsonPath("$.content.description", is("Iphone 6 32G")))
            .andExpect(jsonPath("$.content.price", is((double) 599.99)));
    }

}
