package api.controller;

import api.Application;
import api.domain.Customer;
import api.repository.CustomerRepository;
import api.service.DatabaseInitService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.nio.charset.Charset;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@Transactional
@WebAppConfiguration
public class CustomerControllerTest 
{
    private MediaType contentType = new MediaType(
        MediaType.APPLICATION_JSON.getType(),
        MediaType.APPLICATION_JSON.getSubtype(),
        Charset.forName("utf8")
    );

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;


//    @Mock
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();;
        jdbcTemplate.execute("truncate customers");
        customerRepository.save(new Customer((long) 1, "John", "Woo"));
        customerRepository.save(new Customer((long) 2, "Jeff", "Sunn"));
        customerRepository.save(new Customer((long) 3, "Kait", "Loft"));
        customerRepository.save(new Customer((long) 4, "Blues", "Lee"));
    }

    @Test
    public void findAll() throws Exception 
    {
        this.mockMvc.perform( get("/customers")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.content", hasSize(4)))

            .andExpect(jsonPath("$.content[0].id", is(1)))
            .andExpect(jsonPath("$.content[0].firstName", is("John")))
            .andExpect(jsonPath("$.content[0].lastName", is("Woo")))

            .andExpect(jsonPath("$.content[1].id", is(2)))
            .andExpect(jsonPath("$.content[1].firstName", is("Jeff")))
            .andExpect(jsonPath("$.content[1].lastName", is("Sunn")))

            .andExpect(jsonPath("$.content[2].id", is(3)))
            .andExpect(jsonPath("$.content[2].firstName", is("Kait")))
            .andExpect(jsonPath("$.content[2].lastName", is("Loft")))

            .andExpect(jsonPath("$.content[3].id", is(4)))
            .andExpect(jsonPath("$.content[3].firstName", is("Blues")))
            .andExpect(jsonPath("$.content[3].lastName", is("Lee"))) ;
    }

    @Test
    public void findOne() throws Exception 
    {
        this.mockMvc.perform(get("/customers/1")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            .andExpect(jsonPath("$.content.id", is(1)))
            .andExpect(jsonPath("$.content.firstName", is("John")))
            .andExpect(jsonPath("$.content.lastName", is("Woo")));

        this.mockMvc.perform(get("/customers/4")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            .andExpect(jsonPath("$.content.id", is(4)))
            .andExpect(jsonPath("$.content.firstName", is("Blues")))
            .andExpect(jsonPath("$.content.lastName", is("Lee")));
    }

}
