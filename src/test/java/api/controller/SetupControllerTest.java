package api.controller;

import api.Application;
import api.repository.CustomerRepository;
import api.service.DatabaseInitService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;


import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.web.context.WebApplicationContext;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {Application.class})
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@ActiveProfiles("test")
@WebAppConfiguration
public class SetupControllerTest 
{
    private MockMvc mockMvc;

    @InjectMocks
    SetupController setupController;

    @Autowired
    WebApplicationContext context;

    @Mock
    DatabaseInitService databaseInitService;

    @Mock
    private CustomerRepository customerRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
//        this.mockMvc = MockMvcBuilders.standaloneSetup(setupController).build();
//        this.mockMvc = MockMvcBuilders.standaloneSetup(cController).build();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();;
    }

    @Test
    public void customerDbSetup() throws Exception 
    {
        MvcResult result = this.mockMvc.perform(get("/setup/customer"))
           .andExpect(content().contentType(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.content", is(true)))
           .andReturn();

        String content = result.getResponse().getContentAsString();
//        assertEquals(content, "true");


//           .andExpect(content().string("true"))
//           .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new Boolean(true)));
 //.andExpect(content().string("true"));
//           .andReturn();
//        String content = result.getResponse().getContentAsString();
//        System.out.println(content);
//            .andExpect(status().isOk());
//            .andExpect(content().string("true"));
    }

    @Test
    public void productDbSetup() throws Exception 
    {
        this.mockMvc.perform(get("/setup/product"))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.content", is(true)));
    }

    @Test
    public void orderDbSetup() throws Exception 
    {
       this.mockMvc.perform(get("/setup/order"))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.content", is(true)));
    }
}
