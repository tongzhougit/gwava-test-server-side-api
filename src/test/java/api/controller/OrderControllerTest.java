package api.controller;

import api.Application;
import api.domain.Order;
import api.repository.OrderRepository;
import api.service.DatabaseInitService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.nio.charset.Charset;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class OrderControllerTest 
{
    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();;
        jdbcTemplate.execute("truncate orders");
        orderRepository.save(new Order((long) 1, 1, 909.33));
        orderRepository.save(new Order((long) 2, 3, 5077.99));
        orderRepository.save(new Order((long) 3, 4, 200.20));
        orderRepository.save(new Order((long) 4, 2, 80));
 
    }

    @Test
    public void findAll() throws Exception 
    {
        this.mockMvc.perform( get("/orders")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.content", hasSize(4)))

            .andExpect(jsonPath("$.content[0].id", is(1)))
            .andExpect(jsonPath("$.content[0].customerId", is(1)))
            .andExpect(jsonPath("$.content[0].totalPrice", is((double) 909.33)))

            .andExpect(jsonPath("$.content[1].id", is(2)))
            .andExpect(jsonPath("$.content[1].customerId", is(3)))
            .andExpect(jsonPath("$.content[1].totalPrice", is((double) 5077.99)))

            .andExpect(jsonPath("$.content[2].id", is(3)))
            .andExpect(jsonPath("$.content[2].customerId", is(4)))
            .andExpect(jsonPath("$.content[2].totalPrice", is((double) 200.20)))

            .andExpect(jsonPath("$.content[3].id", is(4)))
            .andExpect(jsonPath("$.content[3].customerId", is(2)))
            .andExpect(jsonPath("$.content[3].totalPrice", is((double) 80)));

    }

    @Test
    public void findOne() throws Exception 
    {
        this.mockMvc.perform(get("/orders/1")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            .andExpect(jsonPath("$.content.id", is(1)))
            .andExpect(jsonPath("$.content.customerId", is(1)))
            .andExpect(jsonPath("$.content.totalPrice", is((double) 909.33)));

        this.mockMvc.perform(get("/orders/3")
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            .andExpect(jsonPath("$.content.id", is(3)))
            .andExpect(jsonPath("$.content.customerId", is(4)))
            .andExpect(jsonPath("$.content.totalPrice", is((double) 200.20)));
    }

}
