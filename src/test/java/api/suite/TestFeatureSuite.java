package api.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import api.controller.SetupControllerTest;
import api.controller.CustomerControllerTest;
import api.controller.ProductControllerTest;
import api.controller.OrderControllerTest;

@RunWith(Suite.class)
@SuiteClasses({
    SetupControllerTest.class, 
    CustomerControllerTest.class,
    ProductControllerTest.class,
    OrderControllerTest.class
})
public class TestFeatureSuite {

}
